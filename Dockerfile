FROM node:11-alpine
ARG env
WORKDIR /usr/src/app
COPY . .
FROM nginx:stable-alpine
WORKDIR /usr/share/nginx/html
COPY --from=0 /usr/src/app .